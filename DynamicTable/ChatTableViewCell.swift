//
//  ChatTableViewCell.swift
//  DynamicTable
//
//  Created by Sarawanak on 9/6/17.
//  Copyright © 2017 Sarawanak. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    @IBOutlet weak var chatLabel: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
