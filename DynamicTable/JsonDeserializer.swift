//
//  JsonDeserializer.swift
//  DynamicTable
//
//  Created by Sarawanak on 9/6/17.
//  Copyright © 2017 Sarawanak. All rights reserved.
//

import Foundation

func getTableData() -> [[String:Any]]? {
    
    if let file = try? Data(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Content", ofType: "json")!)),
        let json = try? JSONSerialization.jsonObject(with: file, options: JSONSerialization.ReadingOptions.mutableContainers) {
        
        let contentArray = (json as? [String: Any])?["content"] as? [[String: Any]]
        
        return contentArray
    }
    else {
        print("ERROR READING File")
    }
    return nil
}
