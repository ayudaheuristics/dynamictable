//
//  ViewController.swift
//  DynamicTable
//
//  Created by Sarawanak on 9/6/17.
//  Copyright © 2017 Sarawanak. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    var dynamicData: [[String:Any]]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 55
        automaticallyAdjustsScrollViewInsets = false
        tableView.register(UINib.init(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "cellId")
        dynamicData = getTableData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollToBottom()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dynamicData!.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as? ChatTableViewCell
        
        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: (dynamicData?[indexPath.row]["text"]) as! String, attributes: [NSFontAttributeName : UIFont.preferredFont(forTextStyle: .body)])
        
        if let hasImage = dynamicData?[indexPath.row]["image"] as? Bool, hasImage {
            let attachment: NSTextAttachment = NSTextAttachment()
            let resizedImage = resizeImage(UIImage(named: "trees.jpg")!)
            attachment.image = resizedImage
            let imgString = NSAttributedString(attachment: attachment)
            attrString.append(imgString)
        }
        cell?.chatLabel.attributedText = attrString
        
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete") { [weak self] (rowAction, forPath) in
            self?.dynamicData?.remove(at: forPath.row)
            tableView.reloadData()
            self?.scrollToBottom()
        }
        return [action]
    }
    
    func scrollToBottom() {
        let indexOfLastRow = IndexPath(row: dynamicData!.count - 1, section: 0)
        tableView.scrollToRow(at: indexOfLastRow, at: .bottom, animated: true)
    }
    
    @IBAction func resetContent(_ sender: Any) {
        dynamicData = getTableData()
        tableView.reloadData()
    }
    
    func resizeImage(_ image: UIImage) -> UIImage {
        let size = getSize()
        let aSize = image.size
        var nSize: CGSize
        
        let  wdRatio = size.width / aSize.width
        let htRatio = size.height / aSize.height
        
        if wdRatio > htRatio {
            nSize = CGSize(width: aSize.width * htRatio, height: aSize.height * htRatio)
        }
        else {
            nSize = CGSize(width: aSize.width * wdRatio, height: aSize.height * wdRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: nSize.width, height: nSize.height)
        
        UIGraphicsBeginImageContextWithOptions(nSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        let nImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return nImg!
    }
    
    func getSize() -> CGSize {
        let rNo = CGFloat(arc4random_uniform(100) + 200)
        return CGSize(width: rNo, height: rNo)
        
    }
    
}

